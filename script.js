// count
db.fruits.aggregate([
    {$match : {
      onSale : true
    }},
    {$group : {
      _id : "$stock",
      fruitsOnSale : {$count : {}}
    }},
    {$project : {
      _id : 0
    }}
  ]).pretty();

// db.fruits.aggregate([{ $match: { onSale: true } }, { $count: "onSale" }]);

// number 3
db.fruits.aggregate([
		{$match : {
		  stock : {$gte : 20}
		}},
    {$group : {
      _id : "$enoughStock",
      enoughStock : {$count : {}}
    }},
    {$project : {
      _id : 0
    }}
  ]).pretty();

// number 5
db.fruits.aggregate([
		{$match : {
		  onSale : true
		}},
    {$group : {
      _id : "$supplier_id",
      avg_price : {$avg : "$price"}
    }}
  ]).pretty();


// number 6
db.fruits.aggregate([
    {$group : {
      _id : "$supplier_id",
      max_price : {$max : "$price"}
    }}
  ]).pretty();

// number 7
db.fruits.aggregate([
    {$group : {
      _id : "$supplier_id",
      min_price : {$min : "$price"}
    }}
  ]).pretty();